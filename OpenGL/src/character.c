#include"../include/character.h"

CHARACTER *createCharacter(int x, int y, int v, Direction d){

	CHARACTER *C = malloc(sizeof(CHARACTER));

	C->x = x;
	C->y = y;
	C->v = v;
	C->d = d;
	C->t = 0;

	return C;

}

void deleteCharacter(CHARACTER **C){

	free(*C);
	*C = NULL;

}

void setCharacterVelocity(CHARACTER *C, int v, int dimension){

	if(C->d % 2 == 0){
		int x = C->x / dimension;
		int rx = C->x % dimension;

		if(rx % v != 0){
			C->x = x*dimension + v*(rx/v);
		}

	}
	else{
		int y = C->y / dimension;
		int ry = C->y % dimension;

		if(ry % v != 0){
			C->y = y*dimension + v*(ry/v);
		}

	}

	C->v = v;
}

void setCharacterX(CHARACTER *C, int x){
	C->x = x;
}

void setCharacterY(CHARACTER *C, int y){
	C->y = y;
}

void setCharacterDirection(CHARACTER *C, Direction d){
	C->d = d;
}

void setCharacterTime(CHARACTER *C, int t){
	C->t = t;
}

int getCharacterVelocity(CHARACTER *C){
	return C->v;
}

int getCharacterX(CHARACTER *C){
	return C->x;
}

int getCharacterY(CHARACTER *C){
	return C->y;
}

Direction getCharacterDirection(CHARACTER *C){
	return C->d;
}

int getCharacterTime(CHARACTER *C){
	return C->t;
}


void decrementTime(CHARACTER *C){
	if(C->t != 0){
		C->t -= 1;
	}
}

int outOfTime(CHARACTER *C){
	return C->t == 0;
}

void walkCharacter(CHARACTER *C){

	switch(C->d){

		case UP    : C->y -= C->v;
			break;
		case DOWN  : C->y += C->v;
			break;
		case RIGHT : C->x += C->v;
			break;
		case LEFT  : C->x -= C->v;
			break;
	}
	
}

void getCharacterVector(CHARACTER *C, int dv[2]){

	switch(C->d){

		case LEFT : 
			dv[0] = -1; 
			dv[1] =  0;
			break;
		case RIGHT :
			dv[0] = 1; 
			dv[1] = 0;
			break;
		case UP :
			dv[0] =  0; 
			dv[1] = -1;
			break;
		case DOWN : 
			dv[0] = 0; 
			dv[1] = 1;
			break;
	}

}

Direction getInvertedDirection(Direction d){

    return (d+2) % 4;

}

void setDirectionBasedOnView(CHARACTER *C, Direction d, int in3d, Direction *myNewDirection){

	if(in3d){
		*myNewDirection = (C->d + d+1) % 4;
	}
	else{
		*myNewDirection = d;
	}

}
