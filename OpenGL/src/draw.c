#include"../include/draw.h"
		
GLuint texture_id[N_TEXTURES];

void drawMapObject(MAP *M, int i, int j){

	switch(getMapPosition(M,i,j)){

		case MAP_W : drawWall(i,j,GRASS_SIDES);
			break;

		case MAP_C : drawSmallPoint(i,j);
			break;

		case MAP_D : drawSmallPoint(i,j);
			break;

		case MAP_G : drawBigPoint(i,j);
			break;

		case MAP_H : drawBigPoint(i,j);
			break;

		case MAP_I : drawWall(i,j,GLASS);
			break;

	}

}

void drawWall(int i, int j,int n_texture){

	int x = N_MAP*(i);
	int y = N_MAP*(j);

	drawCube(x,y,0,0,N_MAP,NULL,1,n_texture);

}

void drawSmallPoint(int i, int j){

	int x = N_MAP*(i);
	int y = N_MAP*(j);

	glDisable(GL_TEXTURE_2D);
	glBegin(GL_POLYGON);
	glColor3f(1,1,0.8);
	glVertex2f(x+N_MAP/3,y+N_MAP/3);
	glVertex2f(x+N_MAP*2/3,y+N_MAP/3);
	glVertex2f(x+N_MAP*2/3,y+N_MAP*2/3);
	glVertex2f(x+N_MAP/3,y+N_MAP*2/3);

	glEnd();
}

void drawBigPoint(int i, int j){

	int x = N_MAP*(i);
	int y = N_MAP*(j);

	glDisable(GL_TEXTURE_2D);
	glBegin(GL_POLYGON);
	glColor3f(1,0.8,1);
	glVertex2f(x+N_MAP/5,y+N_MAP/5);
	glVertex2f(x+N_MAP*4/5,y+N_MAP/5);
	glVertex2f(x+N_MAP*4/5,y+N_MAP*4/5);
	glVertex2f(x+N_MAP/5,y+N_MAP*4/5);
	glEnd();

}

void drawPacman(MAP *M){

	int x = getCharacterX((M->pacman)->C);
	int y = getCharacterY((M->pacman)->C);
	int color[3] = {255.0,255.0,0.0};

	glPushMatrix();
	drawCube(x,y,0,0,N_MAP,color,0,0);

	if(getCharacterX((M->pacman)->C) > (N_MAP*(M->size_x - 1))){
		x = (getCharacterX((M->pacman)->C) - N_MAP*M->size_x);
	 	y = (getCharacterY((M->pacman)->C));
		glBegin(GL_POLYGON);
		glColor3f(1,1,0);
		glVertex2f(x+N_MAP/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP*29/30);
		glVertex2f(x+N_MAP/30,y+N_MAP*29/30);
		glEnd();
	} else if(getCharacterY((M->pacman)->C) > (N_MAP*(M->size_y - 1))){
		x = (getCharacterX((M->pacman)->C));
	 	y = (getCharacterY((M->pacman)->C) - N_MAP*M->size_y);
		glBegin(GL_POLYGON);
		glVertex2f(x+N_MAP/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP*29/30);
		glVertex2f(x+N_MAP/30,y+N_MAP*29/30);
		glEnd();
	}

	drawCharacterEyes(M->pacman->C,x,y);

}

void drawGhost(MAP *M, int i){

	int color[3] = {0,128,255};
	int x = getCharacterX((M->ghosts[i])->C);
 	int y = getCharacterY((M->ghosts[i])->C);

 	glPushMatrix();

 	if(!outOfTime(M->ghosts[i]->C)){
 		drawCube(x,y,0,0,N_MAP,color,0,0);
 	}
 	else{
		drawCube(x,y,0,0,N_MAP,M->ghosts[i]->c,0,0);
	}

	if(getCharacterX((M->ghosts[i])->C) > (N_MAP*(M->size_x - 1))){
		x = (getCharacterX((M->ghosts[i])->C)- N_MAP*M->size_x);
 		y = (getCharacterY((M->ghosts[i])->C));
		glBegin(GL_POLYGON);
		glColor3f(M->ghosts[i]->c[0]/255.0,M->ghosts[i]->c[1]/255.0,M->ghosts[i]->c[2]/255.0);
		glVertex2f(x+N_MAP/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP*29/30);
		glVertex2f(x+N_MAP/30,y+N_MAP*29/30);
		glEnd();
	}
	else if(getCharacterY((M->ghosts[i])->C) > (N_MAP*(M->size_y - 1))){
		x = (getCharacterX((M->ghosts[i])->C));
 		y = (getCharacterY((M->ghosts[i])->C)-N_MAP*M->size_y);
		glBegin(GL_POLYGON);
		glColor3f(M->ghosts[i]->c[0]/255.0,M->ghosts[i]->c[1]/255.0,M->ghosts[i]->c[2]/255.0);
		glVertex2f(x+N_MAP/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP/30);
		glVertex2f(x+N_MAP*29/30,y+N_MAP*29/30);
		glVertex2f(x+N_MAP/30,y+N_MAP*29/30);
		glEnd();
	}

	drawCharacterEyes(M->ghosts[i]->C,x,y);

}

void drawCharacterEyes(CHARACTER *C, int x, int y){

	float margin = N_MAP/6;
	float x1=0;
	float y1=0;
	float x2=0;
	float y2=0;
	int color_white[3] = {255,255,255}; 

	switch(C->d){
		case LEFT : 
			x1 =  0;
			y1 =  0;
			x2 =  0;
			y2 =  N_MAP/2;
			break;
		case RIGHT : 
			x1 =  N_MAP/2;
			y1 =  0;
			x2 =  N_MAP/2;
			y2 =  N_MAP/2;
			break;
		case UP : 
			x1 =  0;
			y1 =  0;
			x2 =  N_MAP/2;
			y2 =  0;
			break;
		case DOWN : 
			x1 =  N_MAP/2;
			y1 =  N_MAP/2;
			x2 =  0;
			y2 =  N_MAP/2;
			break;
	}

	drawCube(x+x1+margin/2,y+y1+margin/2,N_MAP,0,N_MAP/2 - margin,color_white,0,0);
	drawCube(x+x2+margin/2,y+y2+margin/2,N_MAP,0,N_MAP/2 - margin,color_white,0,0);

	glPopMatrix();

}

void drawCube(int x, int y, int z, int h, int g, int color[3], int f, int n_texture){

	
	if(f){

		glDisable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture_id[n_texture]);	
		glBegin(GL_QUADS);
         if(n_texture == GRASS_SIDES){
			glEnd();
			glBindTexture(GL_TEXTURE_2D, texture_id[GRASS_TOP]);	
			 // Top Face
			glBegin(GL_QUADS);
	 		glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y,z);            // x,y,z
	        glTexCoord2f(0.0f, 0.0f); glVertex3f(x+g,y,z);          // x+,y,z
	        glTexCoord2f(1.0f, 0.0f); glVertex3f(x+g,y+g,z);        // x+,y+,z
	        glTexCoord2f(1.0f, 1.0f); glVertex3f(x,y+g,z);          // x,y+,z
			glEnd();
			glBindTexture(GL_TEXTURE_2D, texture_id[GRASS_SIDES]);	
			 // Top Face
			glBegin(GL_QUADS);
        }
        else{
        	// Bottom Face
	        glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y,z);            // x,y,z
	        glTexCoord2f(0.0f, 0.0f); glVertex3f(x+g,y,z);          // x+,y,z
	        glTexCoord2f(1.0f, 0.0f); glVertex3f(x+g,y+g,z);        // x+,y+,z
	        glTexCoord2f(1.0f, 1.0f); glVertex3f(x,y+g,z);          // x,y+,z
        }
		// Left Face
        glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y,z+g+h);        // x,y,z+
        glTexCoord2f(0.0f, 0.0f); glVertex3f(x,y,z);            // x,y,z
        glTexCoord2f(1.0f, 0.0f); glVertex3f(x+g,y,z);          // x+,y,z
        glTexCoord2f(1.0f, 1.0f); glVertex3f(x+g,y,z+g+h);  // x+,y,z+
        // Back Face
        glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y,z+g+h);        // x,y,z+
        glTexCoord2f(0.0f, 0.0f); glVertex3f(x,y,z);            // x,y,z
        glTexCoord2f(1.0f, 0.0f); glVertex3f(x,y+g,z);          // x,y+,z
        glTexCoord2f(1.0f, 1.0f); glVertex3f(x,y+g,z+g+h);  // x,y+,z+
        // Front face
        glTexCoord2f(0.0f, 1.0f); glVertex3f(x+g,y,z+g+h);  // x+,y,z+
        glTexCoord2f(0.0f, 0.0f); glVertex3f(x+g,y,z);          // x+,y,z
        glTexCoord2f(1.0f, 0.0f); glVertex3f(x+g,y+g,z);        // x+,y+,z
        glTexCoord2f(1.0f, 1.0f); glVertex3f(x+g,y+g,z+g+h);    // x+,y+,z+
        // Right Face
        glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y+g,z+g+h);  // x,y+,z+
        glTexCoord2f(0.0f, 0.0f); glVertex3f(x,y+g,z);          // x,y+,z
        glTexCoord2f(1.0f, 0.0f); glVertex3f(x+g,y+g,z);        // x+,y+,z
        glTexCoord2f(1.0f, 1.0f); glVertex3f(x+g,y+g,z+g+h);    // x+,y+,z+

        if(n_texture == GRASS_SIDES){
			glEnd();
			glBindTexture(GL_TEXTURE_2D, texture_id[GRASS_TOP]);	
			 // Top Face
			glBegin(GL_QUADS);
	        glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y,z+g+h);        // x,y,z+
	        glTexCoord2f(0.0f, 0.0f); glVertex3f(x+g,y,z+g+h);  // x+,y,z+
	        glTexCoord2f(1.0f, 0.0f); glVertex3f(x+g,y+g,z+g+h);    // x+,y+,z+
	        glTexCoord2f(1.0f, 1.0f); glVertex3f(x,y+g,z+g+h);  // x,y+,z+
			glEnd();
			glDisable(GL_TEXTURE_2D);
        }
        else{
        	 // Top Face
	        glTexCoord2f(0.0f, 1.0f); glVertex3f(x,y,z+g+h);        // x,y,z+
	        glTexCoord2f(0.0f, 0.0f); glVertex3f(x+g,y,z+g+h);  // x+,y,z+
	        glTexCoord2f(1.0f, 0.0f); glVertex3f(x+g,y+g,z+g+h);    // x+,y+,z+
	        glTexCoord2f(1.0f, 1.0f); glVertex3f(x,y+g,z+g+h);  // x,y+,z+
			glEnd();

			glDisable(GL_TEXTURE_2D);
        }

	}
	else{

		glBegin(GL_QUADS);

		glColor3f(color[0]/255.0,color[1]/255.0,color[2]/255.0);	
		glVertex3f(x,y,z+g+h);
		glVertex3f(x+g,y,z+g+h);
		glVertex3f(x+g,y+g,z+g+h);
		glVertex3f(x,y+g,z+g+h);

		glVertex3f(x,y,z);
		glVertex3f(x+g,y,z);
		glVertex3f(x+g,y+g,z);
		glVertex3f(x,y+g,z);

		glVertex3f(x,y,z+g+h);
		glVertex3f(x,y,z);
		glVertex3f(x,y+g,z);
		glVertex3f(x,y+g,z+g+h);

		glVertex3f(x+g,y,z+g+h);
		glVertex3f(x+g,y,z);
		glVertex3f(x+g,y+g,z);
		glVertex3f(x+g,y+g,z+g+h);

		glVertex3f(x,y,z+g+h);
		glVertex3f(x,y,z);
		glVertex3f(x+g,y,z);
		glVertex3f(x+g,y,z+g+h);

		glVertex3f(x,y+g,z+g+h);
		glVertex3f(x,y+g,z);
		glVertex3f(x+g,y+g,z);
		glVertex3f(x+g,y+g,z+g+h);

		glEnd();

	}

}

void drawMap(MAP *M){

    int *p = malloc(getMapX(M)*getMapY(M)*sizeof(int));
    int  n = 0;

	for(int i=0 ; i<getMapX(M) ; i++){
		for(int j=0 ; j<getMapY(M) ; j++){
			if(getMapPosition(M,i,j) != MAP_W){
				drawMapObject(M,i,j);
				if(i == 0){
					drawPolygon(-N_MAP,j*N_MAP,0,(j+1)*N_MAP,PORTAL);
				}
				else if(i == getMapX(M)-1){
					drawPolygon((getMapX(M))*N_MAP,j*N_MAP,(getMapX(M)+1)*N_MAP,(j+1)*N_MAP,PORTAL);
				}
				else if(j == 0){
					drawPolygon(i*N_MAP,-1*N_MAP,(i+1)*N_MAP,0,PORTAL);
				}
				else if(j == getMapY(M)-1){
					drawPolygon(i*N_MAP,(getMapY(M)*N_MAP),(i+1)*N_MAP,(getMapY(M)+1)*N_MAP,PORTAL);
				}
			}
			else{
			    p[n++] =  i*getMapY(M) + j;
			}
		}
	}

	for(int i=0 ; i<n ; i++){
		drawMapObject(M,p[i]/getMapY(M),p[i]%getMapY(M));
	}
	
	free(p);

}

void drawPolygon(int x1, int y1, int x2, int y2, int n_texture){
	glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture_id[n_texture]);	
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f); glVertex2f(x1,y1);
	glTexCoord2f(1.0f, 0.0f); glVertex2f(x2,y1);
	glTexCoord2f(1.0f, 1.0f); glVertex2f(x2,y2);
	glTexCoord2f(0.0f, 1.0f); glVertex2f(x1,y2);
	glDisable(GL_TEXTURE_2D);
	glEnd();
}

void LoadTexture(char *image, int i)
{	
	Image *TextureImage;
    
	TextureImage = (Image *) malloc(sizeof(Image));
	
	if (TextureImage == NULL) 
	{
		printf("Error allocating space for image");
		exit(1);
	}
	
	//	LoadBMP("../Logo.bmp", TextureImage);
	LoadBMP(image, TextureImage);

	glGenTextures(1, &texture_id[i]);
		
	glBindTexture(GL_TEXTURE_2D, texture_id[i]);
	
	glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage->sizeX, TextureImage->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE, TextureImage->data);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);		// Create The Texture

	free (TextureImage->data);
	free( TextureImage );

}



//--------- loading image BMP

int LoadBMP(char *filename, Image *image) {
    FILE *file;
    unsigned long size;                 /*  size of the image in bytes. */
    unsigned long i;                    /*  standard counter. */
    unsigned short int planes;          /*  number of planes in image (must be 1)  */
    unsigned short int bpp;             /*  number of bits per pixel (must be 24) */
    char temp;                          /*  used to convert bgr to rgb color. */

    /*  make sure the file is there. */
    if ((file = fopen(filename, "rb"))==NULL) {
      printf("File Not Found : %s\n",filename);
      return 0;
    }
    
    /*  seek through the bmp header, up to the width height: */
    fseek(file, 18, SEEK_CUR);

    /*  No 100% errorchecking anymore!!! */

    /*  read the width */    image->sizeX = getint (file);
    
    /*  read the height */ 
    image->sizeY = getint (file);
    
    /*  calculate the size (assuming 24 bits or 3 bytes per pixel). */
    size = image->sizeX * image->sizeY * 3;

    /*  read the planes */    
    planes = getshort(file);
    if (planes != 1) {
	printf("Planes from %s is not 1: %u\n", filename, planes);
	return 0;
    }

    /*  read the bpp */    
    bpp = getshort(file);
    if (bpp != 24) {
      printf("Bpp from %s is not 24: %u\n", filename, bpp);
      return 0;
    }
	
    /*  seek past the rest of the bitmap header. */
    fseek(file, 24, SEEK_CUR);

    /*  read the data.  */
    image->data = (char *) malloc(size);
    if (image->data == NULL) {
	printf("Error allocating memory for color-corrected image data");
	return 0;	
    }

    if ((i = fread(image->data, size, 1, file)) != 1) {
	printf("Error reading image data from %s.\n", filename);
	return 0;
    }

    for (i=0;i<size;i+=3) { /*  reverse all of the colors. (bgr -> rgb) */
      temp = image->data[i];
      image->data[i] = image->data[i+2];
      image->data[i+2] = temp;
    }

    fclose(file); /* Close the file and release the filedes */

    /*  we're done. */
    return 1;
}

static unsigned int getint(FILE *fp)
{
  int c, c1, c2, c3;

  /*  get 4 bytes */ 
  c = getc(fp);  
  c1 = getc(fp);  
  c2 = getc(fp);  
  c3 = getc(fp);
  
  return ((unsigned int) c) +   
    (((unsigned int) c1) << 8) + 
    (((unsigned int) c2) << 16) +
    (((unsigned int) c3) << 24);
}

static unsigned int getshort(FILE* fp)
{
  int c, c1;
  
  /* get 2 bytes*/
  c = getc(fp);  
  c1 = getc(fp);

  return ((unsigned int) c) + (((unsigned int) c1) << 8);
}