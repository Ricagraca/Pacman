#include"../include/ghost.h"

int NUMBER_GHOST = 0;

GHOST *createGhost(int x, int y, int v, Direction d, GhostType t){

	GHOST *G = (GHOST *)calloc(1,sizeof(GHOST));

	G->C = createCharacter(x,y,v,d);
	G->t = t;
	G->c = (int *)malloc(3*sizeof(int));
	setGhostRGB(G);

	NUMBER_GHOST += 1;

	return G;

}

void deleteGhost(GHOST **G){

	if((*G) == NULL){
		return ;
	}	

	deleteCharacter(&(*G)->C);
	free((*G)->c);
	free(*G);
	*G = NULL;
	NUMBER_GHOST -= 1;

}

void setGhostType(GHOST *G, GhostType t){
	G->t = t;
}

GhostType getGhostTypeInt(GHOST *G){
	return G->t;
}

int *getGhostColorRGB(GHOST *G){
	return G->c;
}

void changeGhostColor(GHOST *G, int r, int g, int b){
	G->c[0] = r;
	G->c[1] = g;
	G->c[2] = b;
}

void setGhostRGB(GHOST *G){

	switch(G->t){

		case BLINKY : 
			changeGhostColor(G,255,0,0);
			break;
		case PINKY :
			changeGhostColor(G,255,192,203);
			break;
		case INKY :
			changeGhostColor(G,0,255,255);
			break;
		case CLYDE :
			changeGhostColor(G,255,165,0);
			break;

	}

}

void reviveGhost(GHOST *G){
	G->dead = 0;
}

void killGhost(GHOST *G){
	G->dead = 1;
}

int isGhostDead(GHOST *G){
	return G->dead;
}

char *getGhostTypeString(GHOST *G){
	
	switch(G->t){

		case BLINKY : return "BLINKY";
		case PINKY : return "PINKY";
		case INKY : return "INKY";
		case CLYDE : return "CLYDE";

	}

	return "NONE";

}

void setGhostDefaultTime(GHOST *G){

	switch(G->t){

		case BLINKY : setCharacterTime(G->C, 500);
			break;
		case PINKY : setCharacterTime(G->C, 600);
			break;
		case INKY : setCharacterTime(G->C, 700);
			break;
		case CLYDE : setCharacterTime(G->C, 800);
			break;
	}

}
