#include"../include/map.h"

int eat;
int xi, yi;
int number_points;
int points;

MAP *fileToMatrix(char *filename){

	MAP *map = malloc(sizeof(MAP));
	FILE *f = fopen(filename,"r");
	srand(time(NULL));
	int aux;
	int a, b, c, d;

	number_points = 0;
	eat = 0;
	xi = 0;
	yi = 0;
	points = 0;

	fscanf(f, "%d %d %d %d", &(a), &(b), &(c), &(d));
	map->pacman = createPacman(a*N_MAP,b*N_MAP,c,d);

	map->ghosts = malloc(N_GHOSTS*sizeof(GHOST *));

	for(int i=0 ; i<N_GHOSTS ; i++){
		fscanf(f, "%d %d %d %d", &(a), &(b), &(c), &(d));
		map->ghosts[i] = createGhost(a*N_MAP,b*N_MAP,c,d,i%4);
	}

	fscanf(f, "%d %d", &(xi), &(yi));
	fscanf(f, "%d %d", &(map->size_x), &(map->size_y));
	map->map_name = malloc(MAP_NAME_LIMIT*sizeof(char));
	fread(map->map_name, sizeof(char), MAP_NAME_LIMIT, f);
	map->map = malloc((map->size_y)*sizeof(int *));

	for(int i=0 ; i<map->size_y ; i++){
		map->map[i] = malloc((map->size_x)*sizeof(int));
		for(int j=0 ; j<(map->size_x) ; j++){
			fscanf(f, "%d", &aux);
			map->map[i][j] = aux;
			if(aux < 7 && aux > 2){
				number_points++;
			}
		}
	}

	eat = 0;
	fclose(f);

	return map;

}

void deleteMap(MAP **M){

	if(*M == NULL){
		return ;
	}

	for(int i=0 ; i<(*M)->size_y ; i++){
		free((*M)->map[i]);
	}

	free((*M)->map);
	free((*M)->map_name);
	free((*M));
	deletePacman(&(*M)->pacman);

	for(int i=0 ; i<N_GHOSTS ; i++){
		deleteGhost(&(*M)->ghosts[i]);
	}

	free((*M)->ghosts);

	(*M) = NULL;

}

int canWalk(MAP *M, CHARACTER *C, Direction d){

	int x = getCharacterX(C);
	int y = getCharacterY(C);
	int v = getCharacterVelocity(C);
	int aux;
	decrementTime(C);
	
	switch(d){

		case UP    : 
			if(x % N_MAP == 0){
				aux = getMapPosition(M,x/N_MAP,((y-v+N_MAP*getMapY(M))%(N_MAP*getMapY(M)))/N_MAP);
				return (aux != MAP_W && aux != MAP_I) || (C != M->pacman->C && aux == MAP_I && outOfTime(C));
			}
			break;
		case DOWN  :
			if(x % N_MAP == 0){
				aux = getMapPosition(M,x/N_MAP,((y+v+N_MAP-1)%(N_MAP*getMapY(M)))/N_MAP);
				return (aux != MAP_W && aux != MAP_I) || (C != M->pacman->C && aux == MAP_I && outOfTime(C));
			}
			break;
		case LEFT  :
			if(y % N_MAP == 0){
				aux = getMapPosition(M,((x-v+N_MAP*getMapX(M)) % (N_MAP*getMapX(M)))/N_MAP,y/N_MAP);
				return (aux != MAP_W && aux != MAP_I) || (C != M->pacman->C && aux == MAP_I && outOfTime(C));
			}
			break;
		case RIGHT :
			if(y % N_MAP == 0){
				aux = getMapPosition(M,((x+v+N_MAP-1) % (N_MAP*getMapX(M)))/N_MAP,y/N_MAP);
				return (aux != MAP_W && aux != MAP_I) || (C != M->pacman->C && aux == MAP_I && outOfTime(C));
			}
			break;
	}

	return 0;

}

int position(MAP *M, CHARACTER *C){
	if(C->x % N_MAP == 0 && C->y % N_MAP == 0)
		return getMapPosition(M,getCharacterX(C)/N_MAP,getCharacterY(C)/N_MAP);
	return MAP_J;
}

int getMapX(MAP *M){
	return M->size_x;
}

int getMapY(MAP *M){
	return M->size_y;
}

char *getMapName(MAP *M){
	return M->map_name;
}

int getMapPosition(MAP *M, int x, int y){
	return M->map[y][x];
}

void setMapX(MAP *M, int x){
	M->size_x = x;	
}

void setMapY(MAP *M, int y){
	M->size_y = y;
}

void setMapName(MAP *M, char *name){
	M->map_name = name;
}

void setMapPosition(MAP *M, int x, int y, int value){
	M->map[y][x] = value;
}

void walkCharacterMap(MAP *M, CHARACTER *C){

	walkCharacter(C); 
	setCharacterX(C,(getCharacterX(C)+N_MAP*M->size_x)%(N_MAP*M->size_x));
	setCharacterY(C,(getCharacterY(C)+N_MAP*M->size_y)%(N_MAP*M->size_y));

}

void randomDirection(MAP *M, int i){

	int n = 0;	
	int A[4];
	int dir;

	int p = position(M,M->ghosts[i]->C);
	if(p == MAP_H || p == MAP_D || p == MAP_B){
		dir = getInvertedDirection(getCharacterDirection(M->ghosts[i]->C));
		for(int j=0 ; j<4 ; j++)
			if((dir != j) && canWalk(M,M->ghosts[i]->C,j))
				A[n++] = j;

		setCharacterDirection(M->ghosts[i]->C,A[rand()%n]);
	}

}

void chooseClosestA(MAP *M, int i, int x, int y){

	int p = position(M,M->ghosts[i]->C);

	if(p == MAP_H || p == MAP_D || p == MAP_B){
		int dir = (getCharacterDirection(M->ghosts[i]->C));
		x = x - getCharacterX(M->ghosts[i]->C);
		y = y - getCharacterY(M->ghosts[i]->C);

		if(dir != LEFT && x > 0 && canWalk(M,M->ghosts[i]->C,RIGHT)){
			setCharacterDirection(M->ghosts[i]->C,RIGHT);
		}
		else if(dir != RIGHT && x < 0 && canWalk(M,M->ghosts[i]->C,LEFT)){
			setCharacterDirection(M->ghosts[i]->C,LEFT);
		}
		else if(dir != UP && y > 0 && canWalk(M,M->ghosts[i]->C,DOWN)){
			setCharacterDirection(M->ghosts[i]->C,DOWN);
		}
		else if(dir != DOWN && y < 0 && canWalk(M,M->ghosts[i]->C,UP)){
			setCharacterDirection(M->ghosts[i]->C,UP);
		}
		else{
			randomDirection(M, i);
		}

	}

}

void chooseClosestB(MAP *M, int i, int x, int y){

	int p = position(M,M->ghosts[i]->C);

	if(p == MAP_H || p == MAP_D || p == MAP_B){
		int dir = (getCharacterDirection(M->ghosts[i]->C));
		x = x - getCharacterX(M->ghosts[i]->C);
		y = y - getCharacterY(M->ghosts[i]->C);

		if(dir != UP && y > 0 && canWalk(M,M->ghosts[i]->C,DOWN)){
			setCharacterDirection(M->ghosts[i]->C,DOWN);
		}
		else if(dir != DOWN && y < 0 && canWalk(M,M->ghosts[i]->C,UP)){
			setCharacterDirection(M->ghosts[i]->C,UP);
		}
		else if(dir != LEFT && x > 0 && canWalk(M,M->ghosts[i]->C,RIGHT)){
			setCharacterDirection(M->ghosts[i]->C,RIGHT);
		}
		else if(dir != RIGHT && x < 0 && canWalk(M,M->ghosts[i]->C,LEFT)){
			setCharacterDirection(M->ghosts[i]->C,LEFT);
		}
		else{
			randomDirection(M, i);
		}

	}

}

void moveGhost(MAP *M, int i){

	if(isGhostDead(M->ghosts[i])){
		if(getCharacterX(M->ghosts[i]->C) != xi*N_MAP || getCharacterY(M->ghosts[i]->C) != yi*N_MAP){
			chooseClosestA(M,i,xi*N_MAP,yi*N_MAP);
		}
		else{
			setGhostDefaultTime(M->ghosts[i]);
			reviveGhost(M->ghosts[i]);
			setCharacterVelocity(M->ghosts[i]->C, 5, N_MAP);
		}
	}
	else
	{
		switch(i%4){

			case BLINKY : chooseClosestA(M,i,getCharacterX(M->pacman->C),getCharacterY(M->pacman->C));
				break;
			case PINKY  : chooseClosestB(M,i,getCharacterX(M->pacman->C),getCharacterY(M->pacman->C));
				break;
			case INKY   : randomDirection(M,i);
				break;
			case CLYDE  : chooseClosestA(M,i,getCharacterX(M->pacman->C),getCharacterY(M->pacman->C));
				break;
		}
	}

	if(canWalk(M,M->ghosts[i]->C,getCharacterDirection(M->ghosts[i]->C))){
		walkCharacterMap(M,M->ghosts[i]->C);
	}

}

void movePacman(MAP *M, Direction d){

	int color[2][3] = {{0,0,255},{255,255,255}};
	int x = (M->pacman->C->x + N_MAP / 2)/N_MAP;
	int y = (M->pacman->C->y + N_MAP / 2)/N_MAP;

	if(x < M->size_x && y < M->size_y && getMapPosition(M,x,y) == 3){
		setMapPosition(M,x,y,1);
		points++;
	}

	if(x < M->size_x && y < M->size_y && getMapPosition(M,x,y) == 4){
		setMapPosition(M,x,y,2);
		points++;
	}

	if(x < M->size_x && y < M->size_y && getMapPosition(M,x,y) == 5){
		setMapPosition(M,x,y,1);
		eat = 500;
		points++;
	}

	if(x < M->size_x && y < M->size_y && getMapPosition(M,x,y) == 6){
		setMapPosition(M,x,y,2);
		eat = 500;
		points++;
	}

	if(eat){
		if(eat <= 80){
			for(int i=0 ; i<N_GHOSTS ; i++)
				if(!isGhostDead(M->ghosts[i]))
					changeGhostColor(M->ghosts[i],color[1][0],color[1][1],color[1][2]);
		}
		else if(eat % 20 == 0)
			for(int i=0 ; i<N_GHOSTS ; i++)
				if(!isGhostDead(M->ghosts[i]))
					changeGhostColor(M->ghosts[i],color[(eat % 40) == 0][0],color[(eat % 40) == 0][1],color[(eat % 40) == 0][2]);
		eat -= 1;
	}
	else{
		for(int i=0 ; i<N_GHOSTS ; i++){
			if(!isGhostDead(M->ghosts[i]))
				setGhostRGB(M->ghosts[i]);
		}
	}

	if(canWalk(M,M->pacman->C,d)){
		setCharacterDirection(M->pacman->C,d);
		walkCharacterMap(M,M->pacman->C);
	}
	else if(canWalk(M,M->pacman->C,getCharacterDirection(M->pacman->C))){
		walkCharacterMap(M,M->pacman->C);
	}

}

int squareIntersection(int x1, int y1, int x2, int y2){

	return !(abs(x1-x2) >= N_MAP || abs(y1-y2) >= N_MAP);

}

int pacmanDied(MAP *M){

	int x;
	int y;

	for(int i=0 ; i<N_GHOSTS ; i++){

		x = getCharacterX(M->ghosts[i]->C);
		y = getCharacterY(M->ghosts[i]->C);
		if(squareIntersection(getCharacterX(M->pacman->C),getCharacterY(M->pacman->C),x,y)){
			if(eat != 0){
				killGhost(M->ghosts[i]);
				changeGhostColor(M->ghosts[i],0,255,0);
				setCharacterVelocity(M->ghosts[i]->C, N_MAP/2, N_MAP);
				return 0;
			}
			
			return 1;
		}

	}

	return 0;

}
