#include"../include/game.h"

/*	MADE BY 
	-- Ricardo Graça
	-- André Fazendeiro */

/* ||MY GLOBAL VARIABLES||

   WE GOT (in3d) WHICH TELLS US IF THE USER IS
   IN 3D MODE OR IN "2D" MOED, WE GOT SOME 
   DEFINED ANGLES FOR PERSPECTIVE, WE GOT A 
   MAP (M) VARIABLE WHICH HOLDS ALL THE GAME INFORMATION
   AND (myNewDirection), THE DIRECTION THE USER WISHES TO 
   GO (DON'T DELETE!)	*/

char *string_list[] = { "YOU DIED restart (r)", "YOU WIN restart (r)?"};
int in3d = 1, pid = 0;  
char name_of_map[1000];
int angles1[] = {90,0,-90,180}, angles2[4][2] = {{0,1},{-1,0},{0,-1},{1,0}};
int current_angleA = 0, going_angleA = 0;
float currentB = 0, goingB = 0;
float currentC = 0, goingC = 0;
int count_lives = 0;
MAP *M = NULL;
Direction myNewDirection;

// ||MY GLOBAL VARIABLES||

void draw(){

	extern int number_points;
	extern int points;

	if(count_lives == 0){
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);	// This Will Clear The Background Color To Black
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glColor3f(1.0,0.0,0.0);
		text(-0.5,0.0,0.1,string_list[0],0);
		glutSwapBuffers();
		return ;
	}

	if(number_points == points){
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);	// This Will Clear The Background Color To Black
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glColor3f(0.0,1.0,0.0);
		text(-0.5,0.0,0.1,string_list[1],0);
		glutSwapBuffers();
		return;
	}

	glEnable(GL_TEXTURE_2D);		// Enable Texture Mapping ( NEW )
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);	// This Will Clear The Background Color To Black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glLoadIdentity();			// Reset The Projection Matrix
								
	if(in3d){

		// DRAW MAP IN 3D
		
		int v[2];
		int d = getCharacterDirection(M->pacman->C);
		going_angleA = angles1[d];
		goingB = angles2[d][0];
		goingC = angles2[d][1];

		// GET PACMAN DIRECTION VECTOR

		getCharacterVector(M->pacman->C,v);
		gluPerspective(100, -1.0, 5.0f, N_MAP*20);
		glRotated(current_angleA,0,0,1);
		glRotated(45,currentB,currentC,0);

		// TRANSLATE TO PACMAN'S POSITION 

	    glTranslated(
	    	-getCharacterX(M->pacman->C) - N_MAP/2 + 3*N_MAP*currentC,
	    	-getCharacterY(M->pacman->C) - N_MAP/2 - 3*N_MAP*currentB,
	    	-(6*N_MAP)
		);

		changeAngle(&current_angleA,going_angleA,5);
		changeVector(&currentB,goingB,5,100);
		changeVector(&currentC,goingC,5,100);

    } else{

    	// DRAW MAP IN "2D"

    	glOrtho(
			 -N_MAP,
			 getMapX(M)*N_MAP + N_MAP,
			 getMapY(M)*N_MAP + N_MAP,
			  -N_MAP,
			 0,
			 30*N_MAP
		); 

    }

    // THESE FUNCTIONS ARE WRITTEN IN (draw.h) IMPLEMENTED IN (draw.c)

	// DRAW MAP FIRST AND CHARACTERS AFTER
	// A MAP IS COMPOSED OF DIFFERENT OBJECTS AND WALL,
	// IT WILL START TO PRINT DOES OBJECTS AND 
	// ONLY THEN PRINT THE WALLS

	drawMap(M); 

	// DRAW ALL GHOSTS WITH A SIMPLE FOR
	// CALL A FUNCTION THAT DRAWS A SPECIFIC GHOST

	for(int i=0 ; i<N_GHOSTS ; i++)
		drawGhost(M,i);


	// DRAW PACMAN, PACMAN HAS A (x,y,v,d) ATTRIBUTES
	// THATA WE USE TO MOVE PACMAN AROUND AND DRAW IT

	drawPacman(M);
				
	glMatrixMode(GL_MODELVIEW);
	
	glutSwapBuffers();

}

void keyboard(unsigned char key, int x, int y){

	switch(key){

		// GETS THE DIRECTION BASED ON THE PLAYERS
		// POINT OF VIEW (3D 2D) (setDirectionBasedOnView)

		case 'w' : setDirectionBasedOnView(M->pacman->C,UP,in3d,&myNewDirection);
		    break;

		case 's' : setDirectionBasedOnView(M->pacman->C,DOWN,in3d,&myNewDirection);
		    break;

		case 'd' : setDirectionBasedOnView(M->pacman->C,RIGHT,in3d,&myNewDirection);
		   	break;

		case 'a' : setDirectionBasedOnView(M->pacman->C,LEFT,in3d,&myNewDirection);
		    break;

	    case 'q'  : current_angleA = angles1[myNewDirection];
	    	in3d = !in3d;
		    break;

	    case  27 : 
	    	kill(pid, SIGKILL);
	    	exit(1);
	    	break;

	   	case 'r' : 
	   	   // KILL CURRENT PROCESS PLAYING MUSIC
	       // CONTINUE THE PARENT PROCESS
	       // NEW MAP!
	       // CREATE A NEW CHILD PROCESS TO PLAY THE MUSIC (MacOs not working X Ubunto working +Z)
		   // kill(pid, SIGKILL);
		   // if((pid = fork()) != 0){ 
	   		   init(); 

		   // }
		   // else{ 
		   // 		while(!playMusic("assets/music/Pacman Dubstep Remix.wav")){}
		   // }
		   
		   break;
	}

}

void animation(){

	// IN CASE LEVEL COMPLETED
	extern int number_points;
	extern int points;

	// IN CASE PACMAN DIES

	if(pacmanDied(M)){

		// KILL MUSIC PROCESS ID AND PLAY ANOTHER 
		// ... STILL TO DO

		// kill(pid, SIGKILL);
		// playMusic("assets/music/Jimmy Barnes screaming (Big Enough).wav");
		// glutTimerFunc(ML_SECONDS,animation,1);
		// glutPostRedisplay();
		
		if(count_lives == 0){
			glutTimerFunc(ML_SECONDS,animation,1);
			return ;
		}
		else{
			extern int points;
			int aux = points;
			MAP *m = fileToMatrix(name_of_map);
			for(int i=0 ; i<getMapX(M) ; i++){
				free(m->map[i]);
			}
			free(m->map);
			deletePacman(&(M->pacman));
			for(int i=0 ; i<N_GHOSTS ; i++){
				deleteGhost(&(M->ghosts[i]));
			}
			M->pacman = m->pacman;
			M->ghosts = m->ghosts;

			points = aux;

			count_lives--;
		}
		glutTimerFunc(ML_SECONDS,animation,1);
		return ;

	}

	// MOVE GHOSTS
	// GHOSTS ARE ALWAY MOVING! WHEN THEY FIND AN INTERSECTION
	// THEY WILL CALCULATE THE NEXT MOVE.
	// THIS MOVE CAN BE RANDOM, A SIMPLE HEURISTIC MOVE OR A PATH

	for(int i=0 ; i<N_GHOSTS ; i++){
		moveGhost(M,i);
	}

	// TRY TO MOVE PACMAN
	// GIVEN AN INPUT DIRECTION, THE PLAYER WILL TRY TO WALK TO
	// 'myNewDirection' IF IT IS ABLE TO WALK THERE, IT WILL WALK,
	// IF NOT, IT WILL EITHER WAIT TI'LL IT CAN OR THE PLAYER MUST 
	// SELECT A NEW INPUT INTO THE KEYBOARD OTHERWISE PACMAN STAYS
	// IN THE SAME SPOT, AND PROBABLY DIE IN A FEW COUPLE OF SECONDS

	movePacman(M,myNewDirection);

	// CALL THE ANIMATION FUNCTION RECURSIVELY

	glutTimerFunc(ML_SECONDS,animation,1);

	glutPostRedisplay();

}

void init(){

	// DELETE CURRENT MAP
	deleteMap(&M);

	// CREATE A NEW GAME
	M = fileToMatrix(name_of_map);
	
	// MY DIRECTION IS THE DIRECTION WRITTEN IN THE FILE
	myNewDirection = M->pacman->C->d;

	// SET ANGLE 
	current_angleA = angles1[myNewDirection];

	// LIFES
	count_lives = 3;

}

int main(int argc, char *argv[]){

	char *image[N_TEXTURES] = {
		"./assets/textures/portal.bmp",
		"./assets/textures/s_block_mc.bmp",
		"./assets/textures/t_block_mc.bmp",
		"./assets/textures/glass_mc.bmp"
	};

	// START THE PARENT PROCESS (GAME)
	// INITIATE COMPONENTS (MAP)
	// WINDOW SIZE PORPORTIONAL TO THE BOARD
	// WINDOW POSITION (WRITTEN IN game.h)
	// WINDOW NAME (WRITTEN IN game.h)
	// TYPE OF CURSOR (WRITTEN IN game.h)
	// FUNCTION THAT DISPLAYS 
 	// FUNCTION FOR KEYBOARD INPUTS
 	// FUNCTION FOR THE MOUSE
 	// START ANIMATION

	if((pid = fork()) != 0){
		if(argv[1] == 0){
			strcpy(name_of_map,"assets/level/second_level.txt");
		}
		else{
			strcpy(name_of_map,argv[1]);
		}
		init(); 
		glutInit(&argc,argv);
		glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); 
		glutInitWindowSize(N_MAP*getMapX(M)/10,N_MAP*getMapY(M)/10); 
		glutInitWindowPosition(WINDOW_X,WINDOW_Y); 
		glutCreateWindow(WINDOW_NAME); 
		glutSetCursor(CURSOR_STATUS); 
		glutDisplayFunc(draw); 
		glutKeyboardFunc(keyboard);
		glutTimerFunc(ML_SECONDS,animation,1); 
		for(int i=0 ; i<N_TEXTURES ; i++){
			LoadTexture(image[i],i);
		}
	 	glutMainLoop();
	}
	else{ // START THE CHILD PROCESS (MUSIC)
		while(!playMusic("assets/music/Pacman Dubstep Remix.wav")){}
	}	

	return 0;

}