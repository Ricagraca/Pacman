#include"../include/pacman.h"

PACMAN *createPacman(int x, int y, int v, Direction d){
	
	PACMAN *P = (PACMAN *)malloc(sizeof(PACMAN));

	P->C = createCharacter(x,y,v,d);
	P->score = 0;

	return P;

}

void deletePacman(PACMAN **P){
	
	if((*P) == NULL){
		return ;
	}	

	deleteCharacter(&(*P)->C);
	free(*P);
	*P = NULL;
}

void setPacmanScore(PACMAN *P, int score){
	P->score = score;
}

int getPacmanScore(PACMAN *P){
	return P->score;
}

void incrementPacmanScore(PACMAN *P){
	P->score += 1;
}