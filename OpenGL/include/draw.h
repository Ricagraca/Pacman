#include"map.h"

// IF ALREADY DEFINED
// THEN IT WILL NOT
// DEFINE!

#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#endif

typedef struct Image {
    unsigned long sizeX;
    unsigned long sizeY;
    char *data;
}Image;

#define N_TEXTURES 4

#define PORTAL 0
#define GRASS_SIDES 1
#define GRASS_TOP 2
#define GLASS 3

// ALL FUNCTIONS USED TO DRAW OBJECTS. 
// SOME FUNCTIONS ARE PRIVATE, THIS MEANS
// THAT THEY AREN'T CALLED IN MAIN, ONLY 
// BY OTHER FUNCTIONS FOR SIMPLICITY

void drawMapObject(MAP *M, int i, int j); // DRAW SPECIFIC OBJECT
void drawMap(MAP *M); // DRAW ALL MAP EXCEPT CHARACTERS
void drawWall(int i, int j,int n_texture); // DRAW THE WALLS
void drawSmallPoint(int i, int j); // DRAW SMALL POINTS
void drawBigPoint(int i, int j); // DRAW BIG POINTS
void drawPacman(MAP *M); // DRAW PACMAN
void drawGhost(MAP *M, int i); // DRAW GHOST
void drawCharacterEyes(CHARACTER *C, int x, int y); // DRAW CHARACTERS EYES
void drawCube(int x, int y, int z, int h, int g, int color[3], int f, int n_texture); // DRAW CUBE OF CERTAIN COLOR
void drawPolygon(int x1, int y1, int x2, int y2, int n_texture);

void LoadTexture(char *image, int i);
int LoadBMP(char *filename, Image *image);
static unsigned int getshort(FILE* fp); 
static unsigned int getint(FILE *fp);
