// Apenas para prints debug
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include <SDL2/SDL.h>
#include <signal.h>

// Objetos do jogo
#include"map.h"
#include"draw.h"

// IN CASE THE OPERATING SYSTEM IS MACOS
#ifdef __APPLE__ 
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
// IN CASE THE OPERATIONG SYSTEM IS WINDOWS
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#endif

#define WINDOW_X      0 // WINDOW X POSTION
#define WINDOW_Y      0 // WINDOW Y POSTION
#define WINDOW_NAME "PACMAN 3D" // WINDOW NAME 
#define ML_SECONDS 17 // NUMBER OF MILISECONDS FOR EACH FRAME (17 -=- 1000/60)

#define CURSOR_STATUS 1 // STATUS OF CURSOR, NOT REALLY IMPORTANT 

void changeAngle(int *ang, int ang_to_go, int inc); // CHANGE ANGLE TO ANOTHER IN STEPS
void changeVector(float *ang, float ang_to_go, int inc, int total); // CHANGE VECTOR TO ANOTHER IN STEPS
void my_audio_callback(void *userdata, Uint8 *stream, int len); // AUXILIAR FUNCTION
int playMusic(char *filename); // PLAY MUSIC IN PROCESS
void init(); // START GAME VARIABLES

static Uint8 *audio_pos;
static int audio_len;
int   curr_string = 0;
char Letras[100];
int ii=0;

// FUNCTIONS TO PLAY AUDIO, NOT REALLY IMPORTANT... FOR THIS PROJECT

void my_audio_callback(void *userdata, Uint8 *stream, int len) {
	
	if (audio_len ==0)
		return;

	SDL_memset(stream, 0, len);
	
	len = ( len > audio_len ? audio_len : len );

	SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);
	
	audio_pos += len;
	audio_len -= len;
}

int playMusic(char *filename){

	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return 1;

	static Uint32 wav_length;
	static Uint8 *wav_buffer;
	static SDL_AudioSpec wav_spec; 
	
	if( SDL_LoadWAV(filename, &wav_spec, &wav_buffer, &wav_length) == NULL ){
  		return 1;
	}

	wav_spec.callback = my_audio_callback;
	wav_spec.userdata = NULL;

	audio_pos = wav_buffer;
	audio_len = wav_length;
	
	if ( SDL_OpenAudio(&wav_spec, NULL) < 0 ){
		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
		exit(-1);
	}
	
	SDL_PauseAudio(0);

	while ( audio_len > 0 ) {
		SDL_Delay(100); 
	}
	
	SDL_CloseAudio();
	SDL_FreeWAV(wav_buffer);

	return 0;

}


void changeVector(float *ang, float ang_to_go, int inc, int total){

	if(*ang > ang_to_go + (float)inc/(float)total){
		*ang -= (float)inc/(float)total;
	}
	else if(*ang < ang_to_go - (float)inc/(float)total){
		*ang += (float)inc/(float)total;
	}

}

void changeAngle(int *ang, int ang_to_go, int inc){

	*ang = *ang % 360;

	if(abs(*ang) == 360)
		*ang = 0;

	if(ang_to_go == 0 && *ang >= 270){
		ang_to_go = 360;
	}
	else if(ang_to_go == -90 && *ang >= 180){
		ang_to_go = 270;
	}
	else if(ang_to_go == 180 && *ang <= -90){
		ang_to_go = -180;
	}
	else if(ang_to_go == 90 && *ang <= -180){
		ang_to_go = -270;
	}
	else if(ang_to_go == 0 && *ang <= -270){
		ang_to_go = -360;
	}
	else if(ang_to_go > *ang && *ang - ang_to_go < -270){
		ang_to_go = ang_to_go - 360;
	}
	else if(ang_to_go < *ang && ang_to_go - *ang > 270){
		ang_to_go = 360 + ang_to_go;
	}

	if(*ang < ang_to_go){
		*ang += inc;
	}
	else if(*ang > ang_to_go){
		*ang -= inc;
	}

}

////////////////////////////////////////////////////
// Funcao que desenha uma sequencia de caracteres (texto)
// numa dada posicao (x,y) e com um factor de escala (scale)
void text(GLfloat x, GLfloat y, GLfloat scale, char* texto, ...)
{       // x,y position, scale factor, text
 
        va_list args;
        char buffer[255], *p;
        GLfloat font_scale = 119.05 + 33; //Maximo + minimo
       
        va_start(args, texto);
        vsprintf(buffer, texto, args);
        va_end(args);
       
        glMatrixMode(GL_MODELVIEW);  
        glPushMatrix();
        glLoadIdentity();
       
        glPushAttrib(GL_ENABLE_BIT);
        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_DEPTH_TEST);
 
        glTranslatef(x, y, 0.0);    //Position of text 
        glScalef(scale/font_scale, scale/font_scale, scale/font_scale); //Scale factor
 
        for(p = buffer; *p; p++)
            //fonte + texto (ver GLUT specification)
            glutStrokeCharacter(GLUT_STROKE_ROMAN, *p);  
        glPopAttrib();
       
        glPopMatrix();
}
