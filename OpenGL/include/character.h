#include<stdlib.h>

// SO WE DON'T HAVE PROBLEMS WITH 
// OVERLINKING 

#ifndef CHARACTER_HEADER
#define CHARACTER_HEADER

// DEFINES THE TYPES OF DIRECTION
// WE ASSUME RIGHT = 0 ... CHANGING THIS
// WILL AFFECT THE GAME!!!

typedef enum{
	RIGHT, DOWN, LEFT, UP
}Direction;

// A CHARACTER HAS A POSITION, A VELOCITY
// AND ALSO A DIRECTION, ESSENTIALLY FOR
// THE GAME

typedef struct{
	int       x; // HORIZONTAL POSITION
	int       y; // VERTICAL POSITION
	int       v; // VELOCITY
	Direction d; // DIRECTION
	int       t;
}CHARACTER;

CHARACTER *createCharacter(int x, int y, int v, Direction d); // CREATE CHARACTER
void deleteCharacter(CHARACTER **C); // DELETE CHARACTER

void setCharacterVelocity(CHARACTER *C, int v, int dimension); // SET VELOCITY
void setCharacterX(CHARACTER *C, int x); // SET CHARACTER POSITION IN HORIZONTAL
void setCharacterY(CHARACTER *C, int y); // SET CHARACTER POSITION IN VERTICAL
void setCharacterDirection(CHARACTER *C, Direction d); // SET CHARACTER DIRECTION
void setDirectionBasedOnView(CHARACTER *C, Direction d, int in3d, Direction *myNewDirection); // SETS DIRECTION BASED ON VIEW
void setCharacterTime(CHARACTER *C, int t); // SET CHARACTER TIME

int getCharacterVelocity(CHARACTER *C); // GET CHARACTER VELOCITY
int getCharacterX(CHARACTER *C); // GET CHARACTER POSITION IN HORIZONTAL
int getCharacterY(CHARACTER *C); // GET CHARACTER POSITION IN VERTICAL
Direction getCharacterDirection(CHARACTER *C); // VET CHARACTER DIRECTION
int getCharacterTime(CHARACTER *C); // GET CHARACTER TIME
void getCharacterVector(CHARACTER *C, int dv[2]); // GETS A VECTOR OF THE CHARACTER
Direction getInvertedDirection(Direction d); // GET INVERTED DIRECTION

void walkCharacter(CHARACTER *C); // WALK CHARACTER
void decrementTime(CHARACTER *C); // DECREMENT JAIL TIME
int outOfTime(CHARACTER *C); // SEE IF CHARACTER CAN ESCAPE

#endif

// END OF DEFINITION
