#include"character.h"

// DIFFERENT TYPES OF GHOSTS...
// WE CAN DEFINE OTHERS LATER.

typedef enum{
	BLINKY, PINKY, INKY, CLYDE
}GhostType;

// THE STRUCTURE OF GHOST
// A GHOST IS A CHARACTER JUST LIKE PACMAN
// THAT HAS A COLOR AND A TYPE AND 
// HAS A LIFE (DEAD/ALIVE).

typedef struct{
	GhostType   t;
	CHARACTER  *C;
	int        *c;
	int      dead;
}GHOST;

GHOST *createGhost(int x, int y, int v, Direction d, GhostType t); // CREATE GHOST
void deleteGhost(GHOST **G); // DELETE GHOST

void setGhostType(GHOST *G, GhostType t); // SET GHOST TYPE 
void setGhostRGB(GHOST *G); // SET GHOST COLOR
void changeGhostColor(GHOST *G, int r, int g, int b); // CHANGE GHOST COLOR
void reviveGhost(GHOST *G); // SETS GHOST TO ALIVE
void killGhost(GHOST *G); // SETS GHOST TO DEAD

GhostType getGhostTypeInt(GHOST *G); // GET GHOST TYPE
char *getGhostTypeString(GHOST *G); // GET GHOST TYPE IN STRING FORMAT
int *getGhostColorRGB(GHOST *G); // GET GHOST COLOR, INT ARRAY
int isGhostDead(GHOST *G); // SEE IF GHOST IS DEAD
void setGhostDefaultTime(GHOST *G); // SET GHOST TIME IN JAIL DEFAULT BASE ON TYPE
