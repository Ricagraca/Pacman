#include"character.h"

// PACMAN IS A CHARACTER WITH 
// A SCORE, THE PLAYERS SCORE
// PLAYER IS PACMAN!

typedef struct{
	CHARACTER *C;
	int    score;
}PACMAN;

PACMAN *createPacman(int x, int y, int v, Direction d); // CREATE PACMAN
void deletePacman(PACMAN **P); // DELETE PACMAN
void setPacmanScore(PACMAN *P, int score); // SET PACMAN SCORE
int getPacmanScore(PACMAN *P); // GET PACMAN SCORE
void incrementPacmanScore(PACMAN *P); // INCREMENT PACMAN SCORE
