#include<stdlib.h>
#include<stdio.h>
#include<time.h>

// THIS FILE IS VERY IMPORTANT
// IF ALREADY DEFINED THEN DON'T DEFINE AGAIN...

#ifndef MAP_HEADER
#define MAP_HEADER

#include"pacman.h"
#include"ghost.h"

// THE STRUCT MAP IS A 'GAME', THIS MEANS
// IT HOLDS INFORMATION OF THE MAP, ALL 
// MAP OBJECTS AND CHARACTERS 

typedef struct {
	int     size_x;
	int     size_y;
	int      **map;
	char *map_name;
	PACMAN *pacman;
	GHOST **ghosts;
}MAP;

#define MAP_NAME_LIMIT 16

#define MAP_J -1 // NOT AN ACTUAL MAP POSITION
#define MAP_W  0 // AREA WITH WALL
#define MAP_A  1 // AREA WITHOUT POINTS AND WITHOUT INTERSECTION 
#define MAP_B  2 // AREA WITHOUT POINTS AND WITH    INTERSECTION
#define MAP_C  3 // AREA WITH    POINT AND WITHOUT squareIntersection
#define MAP_D  4 // AREA WITH    POINT AND WITH    INTERSECTION 
#define MAP_G  5 // AREA WITH    BIG POINT AND WITHOUT INTERSECTION
#define MAP_H  6 // AREA WITH    BIG POINT AND WITH    INTERSECTION 
#define MAP_I  7 // AREA WHICH ONLY GHOSTS CAN WALK

#define N_MAP 210
#define N_GHOSTS 4

MAP *fileToMatrix(char *filename); // CREATES THE GAME BASED ON A FILE
void deleteMap(MAP **M); // DELETE A MAP 

int getMapX(MAP *M); // GET MAP SIZE HORIZONTAL
int getMapY(MAP *M); // GET MAP SIZE VERTICAL
int getMapPosition(MAP *M, int x, int y); // GET THE TYPE OF OBJECT IN THE MAP
char *getMapName(MAP *M); // GET MAP NAME

void setMapX(MAP *M, int x); // SET HORIZONTAL SIZE OF MATRIX
void setMapY(MAP *M, int y); // SET VERTICAL SIZE OF MATRIX
void setMapName(MAP *M, char *name); // SET MAP NAME
void setMapPosition(MAP *M, int x, int y, int value); // SET MAP OBJECT IN A SPECTIFIC POSITION

int canWalk(MAP *M, CHARACTER *C, Direction d); // CAN CHARACTER WALK?
int position(MAP *M, CHARACTER *C); // GET THE POSITION OF CHARACTER
int pacmanDied(MAP *M); // DID PACMAN DIED?

void walkCharacterMap(MAP *M, CHARACTER *C); // WALK CHARACTER
void chooseClosestA(MAP *M, int i, int x, int y); // CHOOSES THE DIRECTION THAT WILL GET HIM CLOSER TO (x,y)
void chooseClosestB(MAP *M, int i, int x, int y); // CHOOSES THE DIRECTION THAT WILL GET HIM CLOSER TO (x,y)
void randomDirection(MAP *M, int i);  // WALK GHOST (i) RANDOMLY
void moveGhost(MAP *M, int i); // MOVE GHOST 
void movePacman(MAP *M, Direction d); // MOVE PACMAN

int squareIntersection(int x1, int y1, int x2, int y2); // SEE IF TWO CHARACTERS INTERSECT

#endif
