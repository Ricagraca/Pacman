# Pacman

## Introduction
A little <b>Pacman</b> game written in Javascript using the <b>ThreeJS</b> library and 
another version written in C using the <b>OpenGL</b> library. For the <b>Pacman</b> game written 
in C, you need to have OpenGL installed.

## Link
See the game here:

<a href="http://webx.ubi.pt/~a35864/Pacman">Pacman Link (ThreeJS version)</a>

## Compile
Linux/MacOS:

    make
    
## Execute

Linux/MacOS:

    ./pacman <level-to-play>

## Contact
Author: Graça, Ricardo (ricagraca.student@gmail.com)