/*  MADE BY 
    -- Ricardo Graça
    -- André Fazendeiro */

// Standard Variables
var camera, scene, renderer;

// Pacman Variables
var d=0, pacman = [], pacman_shrink = 0.5, dec = -0.01;

// Ghost Variables
var blinky = [];
var pinky = [];
var clyde = [];
var inky = [];

// Map graphics
var map_graphics = [];

// Camera position
var angles1 = [90,0,-90,180], angles2 = [[0,1],[-1,0],[0,-1],[1,0]];
var current_angleA = [0], going_angleA = 0;
var currentB = [0], goingB = 0;
var currentC = [0], goingC = 0;
var in3d = true;

function changeVector(ang,ang_to_go,inc,total){

    if(ang[0] > ang_to_go + inc/total){
        ang[0] -= inc/total;
    }
    else if(ang[0] < ang_to_go - inc/total){
        ang[0] += inc/total;
    }

}

function changeAngle(ang,ang_to_go,inc){

    ang[0] = ang[0] % 360;

    if(Math.abs(ang[0]) == 360)
        ang[0] = 0;

    if(ang_to_go == 0 && ang[0] >= 270){
        ang_to_go = 360;
    }
    else if(ang_to_go == -90 && ang[0] >= 180){
        ang_to_go = 270;
    }
    else if(ang_to_go == 180 && ang[0] <= -90){
        ang_to_go = -180;
    }
    else if(ang_to_go == 90 && ang[0] <= -180){
        ang_to_go = -270;
    }
    else if(ang_to_go == 0 && ang[0] <= -270){
        ang_to_go = -360;
    }
    else if(ang_to_go > ang[0] && ang[0] - ang_to_go < -270){
        ang_to_go = ang_to_go - 360;
    }
    else if(ang_to_go < ang[0] && ang_to_go - ang[0] > 270){
        ang_to_go = 360 + ang_to_go;
    }

    if(ang[0] < ang_to_go){
        ang[0] += inc;
    }
    else if(ang[0] > ang_to_go){
        ang[0] -= inc;
    }

}

// When user resizes window
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

// Angle, direction
function angleOnDirection(d){
    switch(d){
        case 0 : return Math.PI;
        case 1 : return Math.PI / 2;
        case 2 : return 0;
        case 3 : return -Math.PI / 2;
    }
}

// Animation function
function animate() {

    requestAnimationFrame( animate );

    if(in3d){
        // Some Code
        var di = (map.d + 1) % 4;
        going_angleA = angles1[di];
        goingB = angles2[di][0];
        goingC = angles2[di][1];

        // GET PACMAN DIRECTION VECTOR

        var v = map.characterVector(v);
        camera.rotation.z = current_angleA[0] * (Math.PI)/180;
        camera.rotation.x = currentB[0] * ((Math.PI)/180 - Math.PI/4);
        camera.rotation.y = currentC[0] * ((Math.PI)/180 + Math.PI/4);

        // TRANSLATE TO PACMAN'S POSITION 
        camera.position.z = 3;
        camera.position.x = pacman[0].position.x + 3*currentC[0];
        camera.position.y = pacman[0].position.y + 3*currentB[0];

        changeAngle(current_angleA,going_angleA,5);
        changeVector(currentB,goingB,5,100);
        changeVector(currentC,goingC,5,100);
    }
    else{
        camera.rotation.z = 0;
        camera.rotation.x = 0;
        camera.rotation.y = 0;

        camera.position.z = 10;
        camera.position.x = pacman[0].position.x;
        camera.position.y = pacman[0].position.y;
    }


    // Walk pacman
    // If pacman dies
    pacman[0].scale.set(pacman_shrink,pacman_shrink,pacman_shrink);
    if(map.movePacman(d,map_graphics,scene)){
        if(pacman_shrink > 0 && dec < 0){
            pacman_shrink += dec;
        }
    }
    if(pacman_shrink < 0.5 && dec > 0){
        pacman_shrink += dec;
        map.dead = false;
    }
    else{
        dec = -0.01;
    }

    // Walk ghosts
    for(var i=0 ; i<map.ghosts.length ; i++){
        map.moveGhost(i);
    }

    // Refresh ghosts graphic position 
    blinky[0].position.x = (map.ghosts[0].x / N_MAP);
    blinky[0].position.y = (map.map.length*N_MAP - map.ghosts[0].y - N_MAP) / N_MAP;
    blinky[0].rotation.y = (angleOnDirection(map.ghosts[0].d));
    if(map.eatable == 0)
    blinky[0].material[3].color.set( 0xff150a ); // Change color
    else{
        if(map.eatable % 40 == 0){
            blinky[0].material[3].color.set( 0x0000ff ); // Change color
        }
        else if (map.eatable % 20 == 0){
            blinky[0].material[3].color.set( 0xffffff );
        }
    }

    pinky[0].position.x = (map.ghosts[1].x / N_MAP);
    pinky[0].position.y = (map.map.length*N_MAP - map.ghosts[1].y - N_MAP) / N_MAP;
    pinky[0].rotation.y = (angleOnDirection(map.ghosts[1].d));
    if(map.eatable == 0)
    pinky[0].material[3].color.set( 0xffb6c1 ); // Change color
    else{
        if(map.eatable % 40 == 0){
            pinky[0].material[3].color.set( 0x0000ff ); // Change color
        }
        else if (map.eatable % 20 == 0){
            pinky[0].material[3].color.set( 0xffffff );
        }
    }

    inky[0].position.x = (map.ghosts[2].x / N_MAP);
    inky[0].position.y = (map.map.length*N_MAP - map.ghosts[2].y - N_MAP) / N_MAP;
    inky[0].rotation.y = (angleOnDirection(map.ghosts[2].d));
    if(map.eatable == 0)
    inky[0].material[3].color.set( 0x4e69a2 ); // Change color
    else{
        if(map.eatable % 40 == 0){
            inky[0].material[3].color.set( 0x0000ff ); // Change color
        }
        else if (map.eatable % 20 == 0){
            inky[0].material[3].color.set( 0xffffff );
        }
    }

    clyde[0].position.x = (map.ghosts[3].x / N_MAP);
    clyde[0].position.y = (map.map.length*N_MAP - map.ghosts[3].y - N_MAP) / N_MAP;
    clyde[0].rotation.y = (angleOnDirection(map.ghosts[3].d));
    if(map.eatable == 0)
    clyde[0].material[3].color.set( 0xffa500 ); // Change color
    else{
        if(map.eatable % 40 == 0){
            clyde[0].material[3].color.set( 0x0000ff ); // Change color
        }
        else if (map.eatable % 20 == 0){
            clyde[0].material[3].color.set( 0xffffff );
        }
    }

    // Refresh pacman graphics position
    pacman[0].position.x = (map.x / N_MAP);
    pacman[0].position.y = (map.map.length*N_MAP - map.y - N_MAP) / N_MAP;
    pacman[0].rotation.y = (angleOnDirection(map.d));

    // Set camera view
    // camera.position.z = 10;
    // camera.position.x = pacman[0].position.x;
    // camera.position.y = pacman[0].position.y;

    // Render in loop
    renderer.render( scene, camera );
    
}

// Keyboard function
function onDocumentKeyDown(e){

    console.log(e.keyCode);
    switch(e.keyCode){
        case 81 : in3d = !in3d; break;
        case 87 : if(in3d)d = map.setDirectionBasedOnView(3);else{d = 0;} break; // TOP
        case 68 : if(in3d)d = map.setDirectionBasedOnView(0);else{d = 1;} break; // RIGHT
        case 83 : if(in3d)d = map.setDirectionBasedOnView(1);else{d = 2;} break; // DOWN
        case 65 : if(in3d)d = map.setDirectionBasedOnView(2);else{d = 3;} break; // LEFT
        case 82 : dec = 0.01; break;
    }

}

// Load 
function handle_load(geometry, materials,objeto) {

    //BASIC MESH
    console.log(objeto.length);
    var material = new THREE.MultiMaterial(materials);
    objeto[objeto.length] = new THREE.Mesh(geometry, material);
    scene.add(objeto[objeto.length-1]);
    objeto[objeto.length-1].position.z = 0;
    objeto[objeto.length-1].rotateX(Math.PI/2);
    objeto[objeto.length-1].scale.set(0.5,0.5,0.5)

}


// Start game
function init(){
    
    // Scene
    scene = new THREE.Scene(); 

    // Camera
    camera = new THREE.PerspectiveCamera( 100, window.innerWidth / window.innerHeight, 1, 1000 ); 

    // Renderer
    renderer = new THREE.WebGLRenderer(); 
    renderer.setSize( window.innerWidth, window.innerHeight );

    // Luz 
    var light = new THREE.AmbientLight(0xffffff, 0.1);
    scene.add(light);
    var light2 = new THREE.PointLight(0xffffff, 1.0);
    light2.position.set(map.map[0].length / 2,map.map.length / 2, 5);
    scene.add(light2);

    // Append renderer to html
    document.body.appendChild( renderer.domElement );

    // Floor plane
    var geometry = new THREE.PlaneBufferGeometry( 1.2*map.map[0].length, 1.2*map.map.length, 50, 50 );
    var vertices = geometry.attributes.position.array;
    for ( var i = -1; i < vertices.length; i += 3) {
      vertices[i] = Math.random() * .5;
    }
    geometry.computeVertexNormals();
    var material = new THREE.MeshPhongMaterial( { ambient: 0x000000, color: 0x000044, specular: 0x000044, shininess: 10, shading: THREE.FlatShading } );
    var plane = new THREE.Mesh( geometry, material );
    scene.add( plane );
    plane.position.x = map.map[0].length/2 - 1;
    plane.position.y = map.map.length/2 - 1;
    plane.position.z = -1

    // Add map
    for(var i=0 ; i < map.map.length    ; i++)
    for(var j=0 ; j < map.map[i].length ; j++){

        switch(map.map[i][j]){
            case MAP_J : 
                break; // NOT AN ACTUAL MAP POSITION
            case MAP_W : 
                var geometry = new THREE.BoxGeometry( 1,1,1 ); 
                var material = new THREE.MeshBasicMaterial( 
                {map: new THREE.TextureLoader().load('assets/textures/colorwall2.png'),
                side: THREE.DoubleSide})
                //var material = new THREE.MeshBasicMaterial( { color: 0xff00ff } ); 
                var cube = new THREE.Mesh( geometry, material ); 
                cube.position.x = j;
                cube.position.y = map.map.length - i - 1;
                scene.add( cube );
                map_graphics[i*map.map.length + j] = cube;
                break; // AREA WITH WALL
            case MAP_A : 
                break; // AREA WITHOUT POINTS AND WITHOUT INTERSECTION 
            case MAP_B : 
                break; // AREA WITHOUT POINTS AND WITH    INTERSECTION
            case MAP_C : 
                var geometry = new THREE.SphereGeometry( 0.5,12,12 ); 
                var material = new THREE.MeshBasicMaterial( { color: 0xffffff } ); 
                var sphere = new THREE.Mesh( geometry, material ); 
                sphere.position.x = j;
                sphere.position.y = map.map.length - i - 1;
                sphere.scale.set(0.25,0.25,0.25);
                scene.add( sphere );
                map_graphics[i*map.map.length + j] = sphere;
                break; // AREA WITH    POINT AND WITHOUT squareIntersection
            case MAP_D : 
                var geometry = new THREE.SphereGeometry( 0.5,12,12 ); 
                var material = new THREE.MeshBasicMaterial( { color: 0xffffff } ); 
                var sphere = new THREE.Mesh( geometry, material ); 
                sphere.position.x = j;
                sphere.position.y = map.map.length - i - 1;
                sphere.scale.set(0.25,0.25,0.25);
                scene.add( sphere );
                map_graphics[i*map.map.length + j] = sphere;
                break; // AREA WITH    POINT AND WITH    INTERSECTION 
            case MAP_G : 
                var geometry = new THREE.SphereGeometry( 0.5,12,12 ); 
                var material = new THREE.MeshBasicMaterial( { color: 0xffffff } ); 
                var sphere = new THREE.Mesh( geometry, material ); 
                sphere.position.x = j;
                sphere.position.y = map.map.length - i - 1;
                sphere.scale.set(0.5,0.5,0.5);
                scene.add( sphere );
                map_graphics[i*map.map.length + j] = sphere;
                break; // AREA WITH    BIG POINT AND WITHOUT INTERSECTION
            case MAP_H : 
                var geometry = new THREE.SphereGeometry( 0.5,12,12 ); 
                var material = new THREE.MeshBasicMaterial( { color: 0xffffff } ); 
                var sphere = new THREE.Mesh( geometry, material ); 
                sphere.position.x = j;
                sphere.position.y = map.map.length - i - 1;
                sphere.scale.set(0.5,0.5,0.5);
                scene.add( sphere );
                map_graphics[i*map.map.length + j] = sphere;
                break; // AREA WITH    BIG POINT AND WITH    INTERSECTION 
            case MAP_I : 
                break; // AREA WHICH ONLY GHOSTS CAN 

        }
        
    }

    // Draw pacman
    var loader = new THREE.JSONLoader();
    loader.load('assets/models/pacman_eyes.json',function(a,b){return handle_load(a,b,pacman);});

    // Draw ghosts
    var loader = new THREE.JSONLoader();
    loader.load('assets/models/ghost.json',function(a,b){return handle_load(a,b,blinky);});

    var loader = new THREE.JSONLoader();
    loader.load('assets/models/ghost.json',function(a,b){return handle_load(a,b,pinky);});
    
    var loader = new THREE.JSONLoader();
    loader.load('assets/models/ghost.json',function(a,b){return handle_load(a,b,clyde);});
    
    var loader = new THREE.JSONLoader();
    loader.load('assets/models/ghost.json',function(a,b){return handle_load(a,b,inky);});

    // Callback
    window.addEventListener("resize", onWindowResize, false );
    document.addEventListener("keydown", onDocumentKeyDown, false);

    // Call animation function
    animate();

}

// Get map from json file and start game
$.getJSON( "assets/level/second_map.json", function( data ) {
    map = new Map(data.map,data.nome,data.ghosts,data.origin,data.pacman);
    init();
});
