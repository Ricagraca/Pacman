class GameObject{

	//0 -> top	1 -> right 2 -> down 3 -> left
	
	constructor(x,y,v,d) {
		this.x = x;
		this.y = y;
		this.v = v;
		this.d = d;
	}

	setCharacterVelocity(v,dimension){
		if(this.d % 2 != 0){
			var x = (this.x / dimension) >> 0;
			var rx = (this.x % dimension);
			if(rx % v != 0){
				this.x = x*dimension + v*((rx / v) >> 0);
			}
		}
		else{
			var y = (this.y / dimension) >> 0;
			var ry = (this.y % dimension);
			if(ry % v != 0){
				this.y = y*dimension + v*((ry / v) >> 0);
			}
		}
		this.v = v;
	}

	walkCharacter(){
		switch(this.d){
			case 0 : this.y -= this.v;
					 break;
			case 2 : this.y += this.v;
					 break;
			case 1 : this.x += this.v;
					 break;	
			case 3 : this.x -= this.v;
					 break;		
		}
	}

	characterVector(){

		var dv = [];

		switch(this.d){
			case 1 : 
				dv[0] = -1; dv[1] =  0;
				break;
			case 3 :
				dv[0] =  1; dv[1] =  0;
				break;
			case 0 :
				dv[0] =  0; dv[1] = -1;
				break;
			case 2 : 
				dv[0] =  0; dv[1] =  1;
				break;
		}

		return dv;

	}
	
	setDirectionBasedOnView(d){

		return (this.d + d+1) % 4;

	}

}
