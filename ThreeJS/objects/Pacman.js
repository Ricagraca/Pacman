class Pacman extends GameObject{

	constructor(x,y,v,d) {
		super(x,y,v,d);
		this.score = 0;
		this.dead = false;
		this.lives = 3;
	}

}