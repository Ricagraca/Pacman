
class Ghost extends GameObject{
	
	constructor(x,y,v,d,tipo) {
		super(x,y,v,d);
		this.tp = tipo;
		this.t = 0;
		this.dead = false;
	}

	setGhostDefaultTime(){

		switch(this.tp){

			case 0 : this.t = 500;
				break;
			case 1 : this.t = 600;
				break;
			case 2 : this.t = 700;
				break;
			case 3 : this.t = 800;
				break;
		}

	}

	setGhostDefaultVelocity(dimension){

		switch(this.tp){

			case 0 : this.setCharacterVelocity(15,dimension);
				break;
			case 1 : this.setCharacterVelocity(10,dimension);
				break;
			case 2 : this.setCharacterVelocity(5,dimension);
				break;
			case 3 : this.setCharacterVelocity(7,dimension);
				break;
		}

	}

}