var MAP_J =  -1; // NOT AN ACTUAL MAP POSITION
var MAP_W =   0; // AREA WITH WALL
var MAP_A =   1; // AREA WITHOUT POINTS AND WITHOUT INTERSECTION 
var MAP_B =   2; // AREA WITHOUT POINTS AND WITH    INTERSECTION
var MAP_C =   3; // AREA WITH    POINT AND WITHOUT squareIntersection
var MAP_D =   4; // AREA WITH    POINT AND WITH    INTERSECTION 
var MAP_G =   5; // AREA WITH    BIG POINT AND WITHOUT INTERSECTION
var MAP_H =   6; // AREA WITH    BIG POINT AND WITH    INTERSECTION 
var MAP_I =   7; // AREA WHICH ONLY GHOSTS CAN 
var N_MAP = 210; // SIZE OF EACH BLOCK

// See if two squares intersect
function squareIntersection(x1,y1,x2,y2){
	return !(Math.abs(x1-x2) >= N_MAP || Math.abs(y1-y2) >= N_MAP);
}

class Map extends Pacman{

	constructor(map,nome,ghost,origin,pacman){
		super(pacman[0]*N_MAP,pacman[1]*N_MAP,pacman[2],pacman[3]);
		this.map = map;
		this.nome = nome;
		this.ghosts = [];
		
		// Number of points
		this.points = 0;
	    for(var i=0 ; i<map.lenght ; i++)
    	for(var j=0 ; j<map[i].length ; j++)
		if(map[i][j] > 2 && map[i][j] < 7)
		this.points += 1;

		// Insert Ghosts
	    for(var i=0 ; i<ghost.length ; i++)
		this.ghosts[i] = (new Ghost(ghost[i][0]*N_MAP,ghost[i][1]*N_MAP,ghost[i][2],ghost[i][3],i));

		// Ghosts eatable
		this.eatable = 0;

		this.origin = origin;
	}

	canWalk(gameobject,direction){
		
		var x = gameobject.x;
		var y = gameobject.y;
		var v = gameobject.v;

		if(gameobject instanceof Ghost && gameobject.t != 0){
			gameobject.t -= 1;
		}

		var map_x = this.map[0].length;
		var map_y = this.map.length;

		switch(direction){

			case 0 : 
				if(x % N_MAP == 0){
					var aux = this.mapPosition((x/N_MAP) >> 0, (((y-v+N_MAP*(map_y))%(N_MAP*map_y))/ N_MAP) >> 0);
					return (aux != MAP_W && aux != MAP_I) || (aux == MAP_I && (gameobject instanceof Ghost) && gameobject.t == 0);
				}
				break;
			case 2 : 
				if(x % N_MAP == 0){
					var aux = this.mapPosition((x/N_MAP) >> 0, (((y+v+N_MAP-1)%(N_MAP*map_y))/ N_MAP) >> 0);
					return (aux != MAP_W && aux != MAP_I) || (aux == MAP_I && (gameobject instanceof Ghost) && gameobject.t == 0);
				}
				break;
			case 3 : 
				if(y % N_MAP == 0){
					var aux = this.mapPosition((((x-v+N_MAP*map_x)%(N_MAP*map_x))/N_MAP) >> 0,(y/N_MAP) >> 0);
					return (aux != MAP_W && aux != MAP_I) || (aux == MAP_I && (gameobject instanceof Ghost) && gameobject.t == 0);
				}
				break;
			case 1 : 
				if(y % N_MAP == 0){
					var aux = this.mapPosition((((x+v+N_MAP-1)%(N_MAP*map_x))/N_MAP) >> 0,(y/N_MAP) >> 0);
					return (aux != MAP_W && aux != MAP_I) || (aux == MAP_I && (gameobject instanceof Ghost) && gameobject.t == 0);
				}
				break;

		}

		return false;

	}

	walkCharacterMap(gameobject){
		var size_x = this.map[0].length; 
		var size_y = this.map.length;
		gameobject.walkCharacter();
		gameobject.x = (gameobject.x + N_MAP * size_x) % (N_MAP*size_x);
		gameobject.y = (gameobject.y + N_MAP * size_y) % (N_MAP*size_y);
	}

	mapPosition(x,y){
		var size_x = this.map[0].length; 
		var size_y = this.map.length;

		if(y >= size_y || x >= size_x || y<0 || x<0){
			return MAP_J;
		}

		return this.map[y][x];
	}

	movePacman(direction,map_graphics,scene){

		var x = (((this.x + N_MAP / 2) >> 0) / N_MAP) >> 0;
		var y = (((this.y + N_MAP / 2) >> 0) / N_MAP) >> 0;

		// See if pacman can eat ghosts
		for(var i=0 ; i<this.ghosts.length ; i++){
			if(squareIntersection(this.ghosts[i].x,this.ghosts[i].y,this.x,this.y)){
				if(this.eatable != 0){
					this.ghosts[i].dead = true;
				}
				else{
					this.dead = true;
					map.lives -= 1;
					break;
				}
			}
		}

		if(this.dead){
			return true;
		}

		// Decrement 
		if(this.eatable > 0){
			this.eatable -= 1;
		}

		// Pacman eats small ball
		if(x < this.map[0].length && y < this.map.length && this.mapPosition(x,y) == 3){
			this.map[y][x] = 1;
			this.points -= 1;
			scene.remove(map_graphics[y*this.map.length+x]);
		}

		if(x < this.map[0].length && y < this.map.length && this.mapPosition(x,y) == 4){
			this.map[y][x] = 2;
			this.points -= 1;
			scene.remove(map_graphics[y*this.map.length+x]);
		}

		// Pacman eats big ball
		if(x < this.map[0].length && y < this.map.length && this.mapPosition(x,y) == 5){
			this.map[y][x] = 1;
			this.points -= 1;
			scene.remove(map_graphics[y*this.map.length+x]);
			this.eatable += 600;
		}

		if(x < this.map[0].length && y < this.map.length && this.mapPosition(x,y) == 6){
			this.map[y][x] = 1;
			this.points -= 1;
			scene.remove(map_graphics[y*this.map.length+x]);
			this.eatable += 600;
		}

		if(this.canWalk(this,direction)){
			this.d = direction;
			this.walkCharacterMap(this);
		}
		else if(this.canWalk(this,this.d)){
			this.walkCharacterMap(this);
		}

		return false;

	}

	position(gameobject){
		if(gameobject.x % N_MAP == 0 && gameobject.y % N_MAP == 0)
			return this.mapPosition((gameobject.x/N_MAP) >> 0,(gameobject.y/N_MAP) >> 0);
		return MAP_J;
	}

	randomDirection(i){

		var A = [];
		var p = this.position(this.ghosts[i]);
		
		if(p == MAP_H || p == MAP_D || p == MAP_B){
			var dir = (this.ghosts[i].d + 2) % 4;
			for(var j=0 ; j<4 ; j++){
				if((dir != j) && this.canWalk(this.ghosts[i],j)){
					A.push(j);
				}
			}
			if(A.length > 0)this.ghosts[i].d = A[(Math.random()*A.length) >> 0];
		}
		
	}

	chooseClosestX(i,x,y){

		var p = this.position(this.ghosts[i]);

		if(p == MAP_H || p == MAP_D || p == MAP_B){
			var dir = this.ghosts[i].d;
			x = x - this.ghosts[i].x;
			y = y - this.ghosts[i].y;

			if(dir != 3 && x > 0 && this.canWalk(this.ghosts[i],1)){
				this.ghosts[i].d = 1;
			}
			else if(dir != 1 && x < 0 && this.canWalk(this.ghosts[i],3)){
				this.ghosts[i].d = 3;
			}
			else if(dir != 0 && y > 0 && this.canWalk(this.ghosts[i],2)){
				this.ghosts[i].d = 2;
			}
			else if(dir != 2 && y > 0 && this.canWalk(this.ghosts[i],0)){
				this.ghosts[i].d = 0;
			}
			else{
				this.randomDirection(i);
			}

		}

	}

	chooseClosestY(i,x,y){

		var p = this.position(this.ghosts[i]);

		if(p == MAP_H || p == MAP_D || p == MAP_B){
			var dir = this.ghosts[i].d;
			x = x - this.ghosts[i].x;
			y = y - this.ghosts[i].y;

			if(dir != 0 && y > 0 && this.canWalk(this.ghosts[i],2)){
				this.ghosts[i].d = 2;
			}
			else if(dir != 2 && y > 0 && this.canWalk(this.ghosts[i],0)){
				this.ghosts[i].d = 0;
			}
			else if(dir != 3 && x > 0 && this.canWalk(this.ghosts[i],1)){
				this.ghosts[i].d = 1;
			}
			else if(dir != 1 && x < 0 && this.canWalk(this.ghosts[i],3)){
				this.ghosts[i].d = 3;
			}
			else{
				this.randomDirection(i);
			}

		}

	}

	moveGhost(i){

		if(this.ghosts[i].dead){
			if(this.ghosts[i].x != this.origin[0]*N_MAP || this.ghosts[i].y != this.origin[1]*N_MAP){
				this.ghosts[i].setCharacterVelocity(105,N_MAP);
				this.chooseClosestX(i,this.origin[0]*N_MAP,this.origin[1]*N_MAP);
			}
			else{
				this.ghosts[i].setGhostDefaultTime();
				this.ghosts[i].dead = false;
				this.ghosts[i].setGhostDefaultVelocity(N_MAP);
			}
		}
		else{
			switch(i){
				case 0 : this.chooseClosestX(i,this.x,this.y);
					break;
				case 1 : this.chooseClosestY(i,this.x,this.y);
					break;
				case 2 : this.randomDirection(i);
					break;
				case 3 : this.randomDirection(i);
					break;
			}
		}
		if(this.canWalk(this.ghosts[i],this.ghosts[i].d)){
			this.walkCharacterMap(this.ghosts[i]);
		}

	}

}